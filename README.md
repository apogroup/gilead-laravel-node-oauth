# Laravel Oauth with Node

Concept code

## Installing Laravel

Prerequisites: PHP 7.1.3+, Composer

Mount the `laravel` folder on your host, and make the `public_html`, `www` directory or equivalent point to the `laravel/public` folder.

Duplicate `.env.example` to create `.env`, and populate this with your DB connection info

### Commands

From the parent directory, get the dependencies

```
composer install
```

Create a new key for this install

```
php artisan key:generate
```

Then migrate the database

```
php artisan migrate
```

You should now be able to create users via the default Laravel interface

## Installing Node

Prerequisites: Node.js

Mount the `liveapp` folder on your host. Edit `app.js` where applicable with correct test domains

### Commands

From the parent directory, get the dependencies

```
npm-install
```

Then run

```
node app.js
```

You should now be able to access the node app (default is http://localhost:1337)
