var express = require('express');
var app = express();
var passport = require('passport');
var OAuth2Strategy = require('passport-oauth').OAuth2Strategy;
var request = require('request');
var cookieSession = require('cookie-session');

app.use(cookieSession({
    maxAge: 24 * 60 * 60 * 1000, // One day in milliseconds
    keys: ['QgXC{u5Bak@Z~!:=BQTNtBn3FJg']
}));

app.use(passport.initialize()); // Used to initialize passport
app.use(passport.session()); // Used to persist login sessions

app.get('/', function (req, res) {
	res.send('Hello World');
})

OAuth2Strategy.prototype.userProfile = function (accessToken, done) {

    var options = {
        url: 'http://gileaddemo.loc/api/user',
        headers: {
            'User-Agent': 'request',
            'Authorization': 'Bearer ' + accessToken,
        }
    };

    request(options, callback);

    function callback(error, response, body) {
        if (error || response.statusCode !== 200) {
            return done(error);
        }
        var user = JSON.parse(body);
        return done(null, user);
    }
};

passport.use('gilead-hiv', new OAuth2Strategy({
    authorizationURL: 'http://gileaddemo.loc/oauth/authorize',
    tokenURL: 'http://gileaddemo.loc/oauth/token',
    clientID: '3',
    clientSecret: 'E78OBp26n53Mw9xrkUeHooXtqTk242JASpk4KKva',
    callbackURL: 'http://localhost:1337/auth/callback'
}, function(accessToken, refreshToken, profile, done) {
	done(null, profile); // passes the profile data to serializeUser
}));

// Used to stuff a piece of information into a cookie
passport.serializeUser((user, done) => {
    done(null, user);
});

// Used to decode the received cookie and persist session
passport.deserializeUser((user, done) => {
	done(null, user);
});

// Redirect the user to the OAuth provider for authentication.  When
// complete, the provider will redirect the user back to the application at
//     /auth/provider/callback
app.get('/auth', passport.authenticate('gilead-hiv'));

// The OAuth provider has redirected the user back to the application.
// Finish the authentication process by attempting to obtain an access
// token.  If authorization was granted, the user will be logged in.
// Otherwise, authentication has failed.
app.get(
	'/auth/callback',
	passport.authenticate('gilead-hiv', { successRedirect: '/hello', failureRedirect: '/' })
);

// logged in route
app.get('/hello', isUserAuthenticated, (req, res) => {
	var out = 'Hello '+req.user.name;
	out += '<br>Your email address is '+req.user.email;
	out += '<br><a href="logout">Logout</a>';
    res.send(out);
});

app.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

function isUserAuthenticated(req, res, next) {


    if (req.user) {
        next();
    } else {
        res.send('You must login! <a href="auth">Auth me</a>');
    }
}


app.listen(1337, () => console.log('Example app listening on port 1337!'))



// https://medium.freecodecamp.org/a-quick-introduction-to-oauth-using-passport-js-65ea5b621a
// https://peach.ebu.io/technical/tutorials/tuto-oauth2-client/
// https://www.pveller.com/oauth2-with-passport-10-steps-recipe/
